import Viewer from './lib/viewer.js'

require.config({ paths: { 'vs': 'https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.30.1/min/vs' }});

require(["./vs/editor/editor.main"], () => {
  let viewer = new Viewer(monaco, document.getElementById('container'))

  var content = ''


  for (let section = 0; section < 4; section++) {
    content += `section_start:1636676633:name_${section}\n`

    for (let i = 1; i <= 10000; i++) {
      let color = '\u001b[1;32m'
      if (i % 2 == 0) {
        color = '\u001b[1;34m'
      }
  
      content += `${color}${i}. Doggo ipsum very jealous pupper doge, shooberino clouds ur givin me a spook.\n`
    }

    content += `section_end:1636676635:name_${section}\n`
  }

  viewer.append(content)
  viewer.append('hello world')
});