import Ansi from './ansi.js'

export default class {
  constructor() {
    this.ansi = new Ansi()
    this.reset()
  }

  reset() {
    this.ansi.reset()
    this.offset = 0
    this.decorations = []
    this.sections = {}
    this.buffer = ''
  }

  scan(input) {
    let start = 0
    let offset = 0

    while (offset < input.length) {
      // find ansi csi
      if (input.startsWith('\u001b[', offset)) {
        this._append(input.slice(start, offset))

        let op
        [op, offset] = this._parse_ansi(input, offset + 2)

        if (op.length > 0 && op.endsWith('m')) {
          this.ansi.evaluate(op.slice(0, -1).split(';'))
        }

        start = offset
        continue
      }

      // parse sections
      if (input.startsWith('section_', offset)) {
        this._append(input.slice(start, offset))

        let section
        [section, offset] = this._parse_section(input, offset + 8)

        if (section !== null) {
          this._section(section[0], section[1], section[2])
        }

        start = offset
        continue
      }

      offset++
    }

    this._append(input.slice(start, offset))

    let buffer = this.buffer
    let decorations = this.decorations

    this.buffer = ''
    this.decorations = []

    return [buffer, decorations]
  }

  _append(content) {
    this.buffer += content
    this.offset += content.length

    let classes = this.ansi.getClasses()
    if (classes.length > 0) {
      this.decorations.push({
        range: [ this.offset, this.offset - content.length ],
        options: { inlineClassName: this.ansi.getClasses().join(' ') }
      })
    }
  }
  
  _section(type, time, name) {
    switch (type) {
    case 'start':
      this.sections[name] = {start: this.offset, startTime: time}
      break

    case 'end':
      if (name in this.sections) {
        this.sections[name].end = this.offset
        this.sections[name].endTime = time

        let diff = time - this.sections[name].startTime

        this.decorations.push({
          range: [this.sections[name].start, this.sections[name].end - 1],
          options: { className: 'section', before: { content: `${diff}s`, inlineClassName: 'section-label' } }
        })
      }
      break
    }
  }

  _parse_ansi(input, offset) {
    let start = offset

    // find any number of parameter bytes (0x30-0x3f)
    for (; offset < input.length; offset++) {
      let c = input.charCodeAt(offset)
      if (c >= 0x30 && c <= 0x3f) {
        continue
      }
      break
    }

    // any number of intermediate bytes (0x20–0x2f)
    for (; offset < input.length; offset++) {
      let c = input.charCodeAt(offset)
      if (c >= 0x20 && c <= 0x2f) {
        continue
      }
      break
    }

    // single final byte (0x40–0x7e)
    let c = input.charCodeAt(offset)
    if (c >= 0x40 && c <= 0x7e) {
      offset++
    }

    return [input.slice(start, offset), offset]
  }

  _parse_section(input, offset) {
    let start = offset

    for (; offset < input.length; offset++) {
      if (input[offset] == "\n") {
        offset++
        break
      }
    }

    let section = input.slice(start, offset).split(':')
    if (section.length == 3) {
      return [section, offset]
    }

    return [null, offset]
  }
}
