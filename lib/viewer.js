import Scanner from './scanner.js'

export default class {
  constructor(monaco, el) {
    this.monaco = monaco

    this.monaco.editor.defineTheme('joblog', {
      base: 'vs-dark',
      inherit: true,
      rules: [],
      colors: [],
    });

    this.monaco.editor.setTheme('joblog');

    monaco.languages.register({
      id: 'joblog'
    });

    this.monaco.languages.registerFoldingRangeProvider('joblog', {
      provideFoldingRanges: function(model) {
        let regions = []
        model.getAllDecorations().filter(x => x.options.className == 'section').forEach(x => {
          regions.push({start: x.range.startLineNumber, end: x.range.endLineNumber, kind: monaco.languages.FoldingRangeKind.Region })
        })

        return regions
      }
    })

    this.editor =  this.monaco.editor.create(el, {
      language: 'joblog',
      autoIndent: 'none',
      codeLens: false,
      readOnly: true,
      wordWrap: true,
      contextmenu: false,
      automaticLayout: true,
      occurrencesHighlight: false,
      links: true,
      matchBrackets: 'never',
      quickSuggestions: false,
      renderLineHighlight: "off",
      selectionHighlight: false,
      showDeprecated: false,
      showUnused: false,
      showFoldingControls: 'always',
      minimap: {
        enabled: false
      }
    })
    this.model = this.editor.getModel()
    this.scanner = new Scanner()
  }

  append(content) {
    const t0 = performance.now();

    let [buffer, decorations] = this.scanner.scan(content)

    let position = this.model.getPositionAt(this.scanner.offset)
    const range = new this.monaco.Range(position.lineNumber, position.column, position.lineNumber + 1, 0)
    this.model.applyEdits([{ range: range, text: buffer, forceMoveMarkers: true }], false)

    const t1 = performance.now();

    console.log(`appending took ${t1 - t0} milliseconds.`);

    let delta = [];

    decorations.forEach(decoration => {
      let start = this.model.getPositionAt(decoration.range[0])
      let end = this.model.getPositionAt(decoration.range[1])

      delta.push({
        range: new this.monaco.Range(
          start.lineNumber,
          start.column,
          end.lineNumber,
          end.column
        ),
        options: decoration.options
      })
    })

    this.model.deltaDecorations([], delta)
  }
}