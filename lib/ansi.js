const styles = {
  'bold':      1,
  'italic':    2,
  'underline': 4,
  'conceal':   8,
  'cross':     16
}

const dispatch = {
  0: (p) => p.reset(),

  1: (p) => p.enable('bold'),      21: (p) => p.disable('bold'),      22: (p) => p.disable('bold'),
  3: (p) => p.enable('italic'),    23: (p) => p.disable('italic'),
  4: (p) => p.enable('underline'), 24: (p) => p.disable('underline'),
  8: (p) => p.enable('conceal'),   28: (p) => p.disable('conceal'),
  9: (p) => p.enable('cross'),     29: (p) => p.disable('cross'),

  30:  (p) => p.fg(0),   31: (p) => p.fg(1),   32: (p) => p.fg(2),
  33:  (p) => p.fg(3),   34: (p) => p.fg(4),   35: (p) => p.fg(5),
  36:  (p) => p.fg(6),   37: (p) => p.fg(7),   39: (p) => p.fg(-1),
  40:  (p) => p.bg(0),   41: (p) => p.bg(1),   42: (p) => p.bg(2),
  43:  (p) => p.bg(3),   44: (p) => p.bg(4),   45: (p) => p.bg(5),
  46:  (p) => p.bg(6),   47: (p) => p.bg(7),   49: (p) => p.bg(-1),
  90:  (p) => p.fg(8),   91: (p) => p.fg(9),   92: (p) => p.fg(10),
  93:  (p) => p.fg(11),  94: (p) => p.fg(12),  95: (p) => p.fg(13),
  96:  (p) => p.fg(14),  97: (p) => p.fg(15),  99: (p) => p.fg(-1),
  100: (p) => p.bg(8),  101: (p) => p.bg(9),  102: (p) => p.bg(10),
  103: (p) => p.bg(11), 104: (p) => p.bg(12), 105: (p) => p.bg(13),
  106: (p) => p.bg(14), 107: (p) => p.bg(15), 109: (p) => p.bg(-1),
}


export default class {
  constructor() {
    this.reset()
  }

  reset() {
    this.fgColor = -1
    this.bgColor = -1
    this.styleMask = 0
  }

  enable(flag) {
    this.styleMask |= styles[flag]
  }

  disable(flag) {
    this.styleMask &= styles[flag]
  }

  evaluate(stack) {
    let command = stack.shift()
    if (!command) {
      return
    }

    let op = parseInt(command)
    switch (op) {
      case 38: this.fg256(stack); break;
      case 48: this.bg256(stack); break;
      default:
        op in dispatch && dispatch[op](this)
    }

    this.evaluate(stack)
  }

  fg(color) {
    this.fgColor = color
  }

  bg(color) {
    this.bgColor = color
  }

  fg256(stack) {
    this.fgColor = this.getColor(stack)
  }

  bg256(stack) {
    this.bgColor = this.getColor(stack)
  }

  getColor(stack) {
    if (stack.length < 2 || stack[0] != '5') {
      return ''
    }

    stack.shift()
    let color = parseInt(stack.shift())

    if (color < 0 || color > 255) {
      return ''
    }

    return color
  }

  getClasses() {
    let classes = [];

    if (this.fgColor >= 0) {
      classes.push('color-'+this.fgColor);
    }

    if (this.bgColor >= 0) {
      classes.push('bgcolor-'+this.bgColor);
    }

    for (var x in styles) {
      if ((this.styleMask & styles[x]) != 0) {
        classes.push('xterm-'+x);
      }
    }

    return classes
  }
}
